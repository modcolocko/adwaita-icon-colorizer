#!/usr/bin/python3
from distutils.dir_util import copy_tree

places = ['folder-drag-accept.svg','folder.svg','folder-documents.svg','folder-download.svg','folder-music.svg','folder-pictures.svg','folder-publicshare.svg','folder-remote.svg','folder-templates.svg','folder-videos.svg','user-bookmarks.svg','user-desktop.svg','user-home.svg']
freaks = ["mimetypes/inode-directory.svg","status/folder-open.svg"]

ACCENTCOLOR = "#DC8ADD"
ACCENTBGCOLOR = "#9141AC"

copy_tree('icons','newicons')

ACCENTCOLOR = ACCENTCOLOR.replace('#','')
ACCENTBGCOLOR = ACCENTBGCOLOR.replace('#','')

for src in places:
    src = "places/" + src
    with open(f"icons/scalable/{src}","r") as f:
        txt = f.read()
        txt = txt.replace("438de6",ACCENTBGCOLOR).replace("a4caee",ACCENTCOLOR).replace("428be2",ACCENTBGCOLOR).replace("62a0ea",ACCENTBGCOLOR).replace("afd4ff",ACCENTBGCOLOR).replace("c0d5ea",ACCENTBGCOLOR)
        f.close()
    with open(f"newicons/scalable/{src}","w") as f:
        f.write(txt)
        f.close()

for src in freaks:
    with open(f"icons/scalable/{src}","r") as f:
        txt = f.read()
        txt = txt.replace("438de6",ACCENTBGCOLOR).replace("a4caee",ACCENTCOLOR).replace("428be2",ACCENTBGCOLOR).replace("62a0ea",ACCENTBGCOLOR).replace("afd4ff",ACCENTBGCOLOR).replace("c0d5ea",ACCENTBGCOLOR)
        f.close()
    with open(f"newicons/scalable/{src}","w") as f:
        f.write(txt)
        f.close()
