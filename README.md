# adwaita-icon-colorizer

This is version two of my Adwaita icon colorization script. This time I've updated it so that it uses the new vector graphics instead of applying a filter to the older bitmap ones. It results in much better looking icons and a far simpler script.

![demo image](https://gitlab.com/modcolocko/adwaita-icon-colorizer/-/raw/e7415991a7eb2a6d7710a4f104f039f254d16691/image.png)

## Usage:

- Open the "main.py" file, and change the ACCENTCOLOR and ACCENTBGCOLOR variables to your liking (this lines up with the color names in Gradience)
- Run the script. Which will create a new theme in the "newicons" folder. 
- Copy the newicons folder into /usr/share/icons or ~/.local/share/icons (global vs user's icon folders)
  - The copy.sh script is there as a an easy way to copy into the user folder
- Use a tool such as Gnome-Tweaks to apply the icon theme.
